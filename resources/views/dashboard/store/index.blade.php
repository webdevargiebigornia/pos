{{-- Extends Layout --}}
@extends('layouts.backend')

{{-- Page Title --}}
@section('page-title', 'Admin')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Store panel')

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('admin') !!}
@endsection

{{-- Header Extras to be Included --}}
@section('head-extras')
    @parent
@endsection

@section('content')
    <!-- Small boxes (Stat box) -->
    <div class="row">
      
        <div class="col-lg-7 col-xs-8">
            <!-- small box -->
            <div class="small-box bg-aqua"  style="position: relative; overflow: auto; width: auto; height: 455px;">
                <div class="inner">
                    <h3></h3>
                    <p>Products</p>
                </div>
                <div class="icon smaller">
                    <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                </div>
               
                <div class="box box-solid">
                  <div class="box-body" style="position: relative; overflow: auto; width: auto; height: 333px; margin-top:40px;">
                    <div class="form-group margin-b-4 margin-t-5">
                      <input class="typeahead form-control" type="text" placeholder="Search" id="search" autocomplete="off">
                        <!-- <input type="text" class="form-control" name="name" placeholder="Search" value=""> -->
                    </div>

                    <table class="table table-bordered">
                      <tbody class="search-pills">
                      <tr>
                        
                        <th style="color:black; text-align:center">Look for products...</th>
                       
                      </tr>
                    </tbody>
                  </table>
                  </div> 
                </div>

            </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-5">
        <div class="col-md-12 ingredient-list">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('name') ? ' has-error' : '' }}">
        <div class="box box-warning">
            <div class="box-header row-md-12">
            <label class="box-title"> <i class="fa fa-shopping-cart"></i> Cart <span class="text-sm">4 item(s)</span></label>
              
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding" style="position: relative; overflow: auto; width: auto; height: 350px;">
            <ul class="todo-list">
            <li>
                <div class="form-group" style="padding-bottom:5px;">
                  <!-- todo text -->
                  <p class="text">Product11111</p>
                  <p class="text-sm" style="text-indent: 15px;">askdgas jsnfajsng asdjgn1231231</p>
                  <!-- General tools such as edit or delete-->
                  <div class="pull-right">
                    <a class="label label-primary" style="font-size:15px;"><i class="fa fa-money"></i> 75.00</a>
                    x <input style="width: 3em" class="form-control-sm" type="number" name="quantity" placeholder="qty">
                    = <input style="width: 7em" class="form-control-sm" type="number" name="price" placeholder="price">
                    <a style="margin-left:5px; margin-right:5px;" href="#"><i style="color:red" class="fa fa-trash-o"></i></a>
                  </div>
                <div>
            </li>


            <li>
                <div class="form-group" style="padding-bottom:5px;">
                  <!-- todo text -->
                  <p class="text">Product </p>
                  <p class="text-sm" style="text-indent: 15px;"></p>
                  <!-- General tools such as edit or delete-->
                  <div class="pull-right">
                    <a class="label label-primary" style="font-size:15px;"><i class="fa fa-money"></i> 35.00</a>
                    x <input style="width: 3em" class="form-control-sm" type="number" name="quantity" placeholder="qty">
                    = <input style="width: 7em" class="form-control-sm" type="number" name="price" placeholder="price">
                    <a style="margin-left:5px; margin-right:5px;" href="#"><i style="color:red" class="fa fa-trash-o"></i></a>
                  </div>
                <div>
            </li>

            <li>
                <div class="form-group" style="padding-bottom:5px;">
                  <!-- todo text -->
                  <p class="text">Product 2</p>
                  <p class="text-sm" style="text-indent: 15px;">askdgas jsnfajsng asdjgn1231231asdasdasdasdasd asdasdasda sada asda asda asdas asda asda</p>
                  <!-- General tools such as edit or delete-->
                  <div class="pull-right">
                    <a class="label label-primary" style="font-size:15px;"><i class="fa fa-money"></i> 23.00</a>
                    x <input style="width: 3em" class="form-control-sm" type="number" name="quantity" placeholder="qty">
                    = <input style="width: 7em" class="form-control-sm" type="number" name="price" placeholder="price">
                    <a style="margin-left:5px; margin-right:5px;" href="#"><i style="color:red" class="fa fa-trash-o"></i></a>
                  </div>
                <div>
            </li>
            <li>
                <div class="form-group" style="padding-bottom:5px;">
                  <!-- todo text -->
                  <p class="text">Product 2</p>
                  <p class="text-sm" style="text-indent: 15px;">askdgas jsnfajsng asdjgn1231231asdasdasdasdasd asdasdasda sada asda asda asdas asda asda</p>
                  <!-- General tools such as edit or delete-->
                  <div class="pull-right">
                    <a class="label label-primary" style="font-size:15px;"><i class="fa fa-money"></i> 23.00</a>
                    x <input style="width: 3em" class="form-control-sm" type="number" name="quantity" placeholder="qty">
                    = <input style="width: 7em" class="form-control-sm" type="number" name="price" placeholder="price">
                    <a style="margin-left:5px; margin-right:5px;" href="#"><i style="color:red" class="fa fa-trash-o"></i></a>
                  </div>
                <div>
            </li>
            <li>
                <div class="form-group" style="padding-bottom:5px;">
                  <!-- todo text -->
                  <p class="text">Product 2</p>
                  <p class="text-sm" style="text-indent: 15px;">askdgas jsnfajsng asdjgn1231231asdasdasdasdasd asdasdasda sada asda asda asdas asda asda</p>
                  <!-- General tools such as edit or delete-->
                  <div class="pull-right">
                    <a class="label label-primary" style="font-size:15px;"><i class="fa fa-money"></i> 23.00</a>
                    x <input style="width: 3em" class="form-control-sm" type="number" name="quantity" placeholder="qty">
                    = <input style="width: 7em" class="form-control-sm" type="number" name="price" placeholder="price">
                    <a style="margin-left:5px; margin-right:5px;" href="#"><i style="color:red" class="fa fa-trash-o"></i></a>
                  </div>
                <div>
            </li>
               
              </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
                <button type="button" class="btn btn-red pull-left"><i class="fa fa-undo"></i> Clear</button>
              <button type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-save"></i> Purchase</button>
            </div>
          </div>
        </div>
        <!-- /.form-group -->
    </div>
  </div>
  <!-- ./col -->

  
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cart Summary</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <table class="table table-bordered">
                      <tbody><tr>
                        <th style="width: 10px; color:black;">#</th>
                        <th style="color:black;">Product</th>
                        <th style="color:black;">Description</th>
                        <th style="color:black;">Price x Qty</th>
                        <th style="color:black;">Total Price</th>

                      </tr>
                      <tr>
                        <td style="color:black;">1.</td>
                        <td style="color:black; font-weight:bold">AMOXI (A1)</td>
                        <td style="color:black;">
                          TEST DESCRIPTION
                        </td>
                        <td style="color:black;">
                          10 X 3
                        </td>
                        <td style="color:black;">
                          30
                        </td>
                      </tr>

                      <tr>
                        <td style="color:black;">2.</td>
                        <td style="color:black; font-weight:bold">BIOG (B23)</td>
                        <td style="color:black;">
                          TEST DESCRIPTION
                        </td>
                        <td style="color:black;">
                          7 X 3
                        </td>
                        <td style="color:black;">
                          21
                        </td>
                      </tr>

                    </tbody>
                    <tfoot>
                    <tr>
                        <td><b>TOTAL:</b></td>
                        <td></td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                          51
                        </td>
                      </tr>
                    </tfoot>
                  </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Confirm</button>
      </div>
    </div>
  </div>
</div>

@endsection

{{-- Footer Extras to be Included --}}
@section('footer-extras')

@endsection


@push('footer-scripts')
<script>
  var path = "{{ route('autocomplete') }}";
    $('input.typeahead').typeahead({
        source:  function (query, process) {
        return $.get(path, { query: query }, function (data) {
                return process(data);
            });
        }
    });


    $('#search').bind("enterKey",function(e){
      var name  = $("#search").val();
      $.ajax({
            type: 'GET',
            url: '/api/ajax/search/product',
            data: { 
                'name' : name,
            },
            success: function(record){
              // alert(record);
              $(".search-pills").html("");
              $(".search-pills").html( '<tr><th style="width: 10px; color:black;">#</th><th style="color:black;">Product</th><th style="color:black;">Description</th><th style="width: 40px; color:black;">Action</th></tr>');

              for (const [key, value] of Object.entries(record)) {
                $(".search-pills").append('<tr>'+
                    '<td style="color:black;">'+`${value['id']}`+'</td>'+
                    '<td style="color:black;">'+`${value['name']}`+'</td>'+
                    '<td style="color:black;">'+`${value['description']}`+'</td>'+
                    '<td style="color:black;"> <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addProduct'+`${value['id']}`+'">Add</button>'+
                    '<div class="modal fade" tabindex="-1" role="dialog" id="addProduct'+`${value['id']}`+'" aria-labelledby="addProductLabel" aria-hidden="true">'+
                      '<div class="modal-dialog" role="document">'+
                        '<div class="modal-content">'+
                          '<div class="modal-header">'+
                            '<h5 class="modal-title">Add to Cart</h5>'+
                            '<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
                              '<span aria-hidden="true">&times;</span>'+
                            '</button>'+
                          '</div>'+
                          '<div class="modal-body">'+
                            '<p><strong>PRODUCT DETAILS:</strong></p>'+
                            '<p>Name: '+`${value['name']}`+'</p>'+
                            '<p>Description: '+`${value['description']}`+'</p>'+
                            '<div class="container-fluid">'+
                            '<hr>'+
                            '<div class="row">'+
                              '<label for="quantity">Quantity</label>'+
                              '<input type="number" class="form-control" id="quantity" name="quantity">'+
                            '</div>'+
                            '</div>'+
                          '</div>'+
                          '<div class="modal-footer">'+
                            '<button type="button" class="btn btn-primary">Save changes</button>'+
                            '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>'+
                          '</div>'+
                        '</div>'+
                      '</div>'+
                    '</div> </td>'+
                '</tr>'
                );
              }
              
            },
            error: function(xhr, status, error){

            }
        });
    });

    $('#search').keyup(function(e){
      if(e.keyCode == 13)
      {
          $(this).trigger("enterKey");
      }
    });
</script>
@endpush
