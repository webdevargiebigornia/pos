<!-- Modal -->
<div class="modal fade" id="addIngredient" style="display: none;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add Ingredient</h4>
              </div>
      <div class="modal-body">
         <div class="form-group">
            <label for="item_no" class="col-form-label">Item No:</label>
            <div class="row">
             <div class="form-group-center col-md-8">
                <input type="text" class="form-control col-6" id="item_no" name="item_no">
             </div> 
             <div class="form-group-center col-md-4">
                <button type="button" class="btn btn-primary search-ingredient">Search</button>
             </div>  
            </div>
          </div>

          <div class="form-group ingredient-details">
          
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary add-ingredient" disabled>Add</button>
      </div>
    </div>
  </div>
</div>

@push('footer-scripts')
<script>
    $(".search-ingredient").click(function(){
        var item_no = $('#item_no').val();
        var selected_items = $("input[name='item_no_list[]']").map(function(){return $(this).val();}).get();

        $.ajax({
            type: 'GET',
            url: '/api/ajax/search/item',
            data: { 
                'selected_items' : selected_items,
                'item_no': item_no,
            },
            success: function(record){
                $('.add-ingredient').prop('disabled',false);
                $(".ingredient-details").html("<hr><p><b>Item Details:</b></p><br><div style='margin-left:20px;'><p>Item no: "+record['item_no']+"</p><p>Name: "+record['name']+"</p><p>Category: "+record['category']['name']+"</p><p>Supplier: "+record['supplier']['name']+"</p></div>");
            },
            error: function(xhr, status, error){
                var err = JSON.parse(xhr.responseText);
                $('.add-ingredient').prop('disabled',true);
                $(".ingredient-details").html("<hr><p><b>Item Details:</b></p><br><div style='margin-left:20px;'><p style='color:red'>"+err+"</p>");
            }

        });
    });

</script>
@endpush