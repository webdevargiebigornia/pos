
@include('admin.products.modals.items.add')
<div class="col-md-7">
    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name">Name *</label>
            <input type="text" class="form-control" name="name" placeholder="Name" value="{{ old('name', $record->name) }}" required>

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('product_category_id') ? ' has-error' : '' }}">
            <label for="product_category_id">Category *</label>
            <select class="form-control" name="product_category_id" required>
                <option disabled>Choose Category...</option>
                @foreach($product_categories as $product_category)
                <option value="{{$product_category->id}}"{{ old('product_category_id', $record->product_category_id) == $product_category->id  ? 'selected' : '' }}>{{$product_category->name}}</option>
                @endforeach
            </select>
            
            @if ($errors->has('product_category_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('product_category_id') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('price') ? ' has-error' : '' }}">
            <label for="price">Price *</label>
            <input type="number" class="form-control" name="price" placeholder="Price" value="{{ old('price', $record->price) }}" required>

            @if ($errors->has('price'))
                <span class="help-block">
                    <strong>{{ $errors->first('price') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>

    <div class="col-md-12 ingredient-list">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('name') ? ' has-error' : '' }}">
        <div class="box">
            <div class="box-header row-md-12">
            <label class="box-title">Items</label>
            <a href="#" data-toggle="modal" data-target="#addIngredient" class="btn btn-xs btn-primary margin-r-5 margin-l-5 pull-right">
                <i class="fa fa-plus"></i> <span> Add</span>
            </a>
              
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table id="ingredient-list" class="table table-striped">
                  <thead>
                  <tr>
                  <th style="width: 10px">#</th>
                  <th>Name</th>
                  <th>Quantity</th>
                  <th style="width: 40px">Action</th>
                </tr>
                  </thead>
                <tbody>
                @if($record)
                    @foreach($record->ingredients as $ingredient)
                    <tr id="item-{{$ingredient->id}}">
                    
                  <td>{{$ingredient->item->item_no}}<input class="form-control-sm" type="hidden" name="item_no_list[]" id="item_no" value="{{$ingredient->item->item_no}}"></td>
                  <td>{{$ingredient->item->name}}<input class="form-control-sm" type="hidden" name="item_id[]" id="item_id" value="{{$ingredient->item_id}}"></td>
                  <td>
                      <input class="form-control-sm" type="number" id="quantity" name="quantity[]" value="{{$ingredient->quantity}}" required>
                  </td>
                  <td>
                    <a class="btn btn-xs btn-danger margin-r-5 margin-l-5 pull-right delete-item" id="{{$ingredient->id}}">
                        <i class="fa fa-times"></i> <span> Remove</span>
                    </a>
                  </td>
                </tr>
                @endforeach
                @endif  
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.form-group -->
    </div>

</div>

<!-- /.col-md-7 -->
@push('footer-scripts')
<script>
    function clearModalInputs(){
        $('#item_no').val('');
        $(".ingredient-details").html('');
        $('.add-ingredient').prop('disabled',true);
    }
    $(".add-ingredient").click(function(){
        var item_no = $('#item_no').val();
        $.ajax({
            type: 'GET',
            url: '/api/ajax/search/item',
            data: { 
                'item_no': item_no,
            },
            success: function(record){
                $('.add-ingredient').prop('disabled',false);
                $('#ingredient-list tbody').append(
                    '<tr id="item-'+record['id']+'">'+
                    
                  '<td>'+record['item_no']+'<input class="form-control-sm" type="hidden" name="item_no_list[]" id="item_no" value="'+record['item_no']+'"></td>'+
                  '<td>'+record['name']+'<input class="form-control-sm" type="hidden" name="item_id[]" id="item_id" value="'+record['id']+'">'+'</td>'+
                  '<td>'+
                      '<input class="form-control-sm" type="number" id="quantity" name="quantity[]" required>'+
                  '</td>'+
                  '<td>'+
                    '<a class="btn btn-xs btn-danger margin-r-5 margin-l-5 pull-right delete-item" id="'+record['id']+'">'+
                        '<i class="fa fa-times"></i> <span> Remove</span>'+
                    '</a>'+
                  '</td>'+
                '</tr>');
                $('#addIngredient').modal('hide');
                clearModalInputs();
            }
        });
    });


    $("#ingredient-list").on('click', '.delete-item', function(){
        $('#item-'+$(this).attr('id')).remove();
    });

</script>
@endpush
