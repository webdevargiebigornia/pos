<div class="col-md-7">
    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('item_no') ? ' has-error' : '' }}">
            <label for="name">Item no *</label>
            <input type="text" class="form-control" name="item_no" placeholder="Item No" value="{{ old('item_no', $record->item_no) }}" required>

            @if ($errors->has('item_no'))
                <span class="help-block">
                    <strong>{{ $errors->first('item_no') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name">Name *</label>
            <input type="text" class="form-control" name="name" placeholder="Name" value="{{ old('name', $record->name) }}" required>

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('supplier_id') ? ' has-error' : '' }}">
            <label for="supplier_id">Supplier *</label>
            <select class="form-control" name="supplier_id" required>
                <option disabled>Choose Supplier...</option>
                @foreach($suppliers as $supplier)
                <option value="{{$supplier->id}}" {{ old('supplier_id', $record->supplier_id) == $supplier->id  ? 'selected' : '' }}>{{$supplier->name}}</option>
                @endforeach
            </select>
            
            @if ($errors->has('supplier_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('supplier_id') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('category_id') ? ' has-error' : '' }}">
            <label for="category_id">Category *</label>
            <select class="form-control" name="category_id" required>
                <option disabled>Choose Category...</option>
                @foreach($categories as $category)
                <option value="{{$category->id}}"{{ old('category_id', $record->category_id) == $category->id  ? 'selected' : '' }}>{{$category->name}}</option>
                @endforeach
            </select>
            
            @if ($errors->has('category_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('category_id') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('expiry_date') ? ' has-error' : '' }}">
            <label for="expiry_date">Expiry Date</label>
            <input type="date" class="form-control" name="expiry_date" placeholder="Expiry Date" value="{{ old('expiry_date', $record->expiry_date) }}">

            @if ($errors->has('expiry_date'))
                <span class="help-block">
                    <strong>{{ $errors->first('expiry_date') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('date_received') ? ' has-error' : '' }}">
            <label for="date_received">Date Received *</label>
            <input type="date" class="form-control" name="date_received" placeholder="Date Received" value="{{ old('date_received', $record->date_received) }}" required>

            @if ($errors->has('date_received'))
                <span class="help-block">
                    <strong>{{ $errors->first('date_received') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

   
    <div class="col-md-12">
    <hr>
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('quantity') ? ' has-error' : '' }}">
            <label for="quantity">Quantity *</label>
            <input type="number" min="1" class="form-control" name="quantity" placeholder="Qty" value="{{ old('quantity', $record->quantity) }}" required>

            @if ($errors->has('quantity'))
                <span class="help-block">
                    <strong>{{ $errors->first('quantity') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('unit_id') ? ' has-error' : '' }}">
            <label for="unit_id">Unit *</label>
            <select class="form-control" name="unit_id" required>
                <option disabled>Choose Unit Type...</option>
                @foreach($units as $unit)
                <option value="{{$unit->id}}"{{ old('unit_id', $record->unit_id) == $unit->id  ? 'selected' : '' }}>{{$unit->name}}</option>
                @endforeach
            </select>
            
            @if ($errors->has('unit_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('unit_id') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('cost') ? ' has-error' : '' }}">
            <label for="cost">Cost/Qty *</label>
            <input type="number" class="form-control" name="cost" placeholder="Cost" value="{{ old('cost', $record->cost) }}" required>
            <input type="number" class="form-control" name="cost_per_quantity" placeholder="Qty" value="{{ old('cost_per_quantity', $record->cost_per_quantity) }}" required>
            @if ($errors->has('cost'))
                <span class="help-block">
                    <strong>{{ $errors->first('cost') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>

</div>
<!-- /.col-md-7 -->
