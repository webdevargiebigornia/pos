<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>
    <li class="{{ \App\Utils::checkRoute(['dashboard::index', 'admin::index']) ? 'active': '' }}">
        <a href="{{ route('dashboard::index') }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
    </li>

    <li class="{{ \App\Utils::checkRoute(['dashboard::index', 'dashboard::store.index']) ? 'active': '' }}">
        <a href="{{ route('dashboard::store') }}">
            <i class="fa fa-store"></i> <span>Store</span>
        </a>
    </li>

    @if (Auth::user()->can('viewList', \App\User::class))
        <li class="{{ \App\Utils::checkRoute(['admin::users.index', 'admin::users.create']) ? 'active': '' }}">
            <a href="{{ route('admin::users.index') }}">
                <i class="fa fa-user-secret"></i> <span>Users</span>
            </a>
        </li>
    @endif

    @if (Auth::user()->can('viewList', \App\Supplier::class) || Auth::user()->can('viewList', \App\Unit::class) || Auth::user()->can('viewList', \App\Category::class) || Auth::user()->can('viewList', \App\ProductCategory::class))
    <li class="treeview {{ \App\Utils::checkRoute(['admin::suppliers.index', 'admin::suppliers.create','admin::units.index', 'admin::units.create','admin::categories.index', 'admin::categories.create']) ? 'active': '' }}">
        <a href="#"><i class="fa fa-cogs"></i> <span>Referentials</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
        <ul class="treeview-menu">
            @if (Auth::user()->can('viewList', \App\Supplier::class))
                <li class="{{ \App\Utils::checkRoute(['admin::suppliers.index', 'admin::suppliers.create']) ? 'active': '' }}">
                    <a href="{{ route('admin::suppliers.index') }}">
                        <i class="fa fa-truck"></i> <span>Suppliers</span>
                    </a>
                </li>
            @endif

            @if (Auth::user()->can('viewList', \App\Unit::class))
                <li class="{{ \App\Utils::checkRoute(['admin::units.index', 'admin::units.create']) ? 'active': '' }}">
                    <a href="{{ route('admin::units.index') }}">
                        <i class="fa fa-cubes"></i> <span>Units</span>
                    </a>
                </li>
            @endif

            @if (Auth::user()->can('viewList', \App\Category::class))
                <li class="{{ \App\Utils::checkRoute(['admin::categories.index', 'admin::categories.create']) ? 'active': '' }}">
                    <a href="{{ route('admin::categories.index') }}">
                        <i class="fa fa-sitemap"></i> <span>Categories</span>
                    </a>
                </li>
            @endif

            @if (Auth::user()->can('viewList', \App\ProductCategory::class))
                <li class="{{ \App\Utils::checkRoute(['admin::product_categories.index', 'admin::product_categories.create']) ? 'active': '' }}">
                    <a href="{{ route('admin::product_categories.index') }}">
                        <i class="fa fa-sitemap"></i> <span>Product Categories</span>
                    </a>
                </li>
            @endif
        </ul>
      </li>
      @endif

        @if (Auth::user()->can('viewList', \App\Item::class))
                <li class="{{ \App\Utils::checkRoute(['admin::items.index', 'admin::items.create']) ? 'active': '' }}">
                    <a href="{{ route('admin::items.index') }}">
                        <i class="fa fa-cube"></i> <span>Items</span>
                    </a>
                </li>
        @endif

        @if (Auth::user()->can('viewList', \App\Product::class))
                <li class="{{ \App\Utils::checkRoute(['admin::products.index', 'admin::products.create']) ? 'active': '' }}">
                    <a href="{{ route('admin::products.index') }}">
                        <i class="fa fa-shopping-bag"></i> <span>Products</span>
                    </a>
                </li>
        @endif


        <li class="treeview {{ \App\Utils::checkRoute(['admin::suppliers.index', 'admin::suppliers.create','admin::units.index', 'admin::units.create','admin::categories.index', 'admin::categories.create']) ? 'active': '' }}">
        <a href="#"><i class="fa fa-area-chart"></i> <span>Reports</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
        <ul class="treeview-menu">
            @if (Auth::user()->can('viewList', \App\Supplier::class))
                <li class="{{ \App\Utils::checkRoute(['admin::suppliers.index', 'admin::suppliers.create']) ? 'active': '' }}">
                    <a href="{{ route('admin::suppliers.index') }}">
                        <i class="fa fa-truck"></i> <span>Suppliers</span>
                    </a>
                </li>
            @endif

            @if (Auth::user()->can('viewList', \App\Unit::class))
                <li class="{{ \App\Utils::checkRoute(['admin::units.index', 'admin::units.create']) ? 'active': '' }}">
                    <a href="{{ route('admin::units.index') }}">
                        <i class="fa fa-cubes"></i> <span>Units</span>
                    </a>
                </li>
            @endif

            @if (Auth::user()->can('viewList', \App\Category::class))
                <li class="{{ \App\Utils::checkRoute(['admin::categories.index', 'admin::categories.create']) ? 'active': '' }}">
                    <a href="{{ route('admin::categories.index') }}">
                        <i class="fa fa-sitemap"></i> <span>Categories</span>
                    </a>
                </li>
            @endif

            @if (Auth::user()->can('viewList', \App\ProductCategory::class))
                <li class="{{ \App\Utils::checkRoute(['admin::product_categories.index', 'admin::product_categories.create']) ? 'active': '' }}">
                    <a href="{{ route('admin::product_categories.index') }}">
                        <i class="fa fa-sitemap"></i> <span>Product Categories</span>
                    </a>
                </li>
            @endif
        </ul>
      </li>
</ul>
