<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Models\Impersonator;
use App\Traits\Eloquent\OrderableTrait;
use App\Traits\Eloquent\SearchLikeTrait;
use App\Traits\Models\FillableFields;
use Illuminate\Notifications\Notifiable;

class Item extends Model
{
    //
    use Notifiable, FillableFields, OrderableTrait, SearchLikeTrait, Impersonator;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'item_no',
        'name',
        'supplier_id',
        'unit_id',
        'category_id',
        'expiry_date',
        'date_received',
        'cost',
        'cost_per_quantity',
        'quantity',
        'remarks',
    ];

    /**
     * @return mixed
     */
    public function getRecordTitle()
    {
        return $this->name;
    }

    public function category(){
        return $this->hasOne(Category::class, 'id' , 'category_id');
    }

    public function unit(){
        return $this->hasOne(Unit::class, 'id', 'unit_id');
    }

    public function supplier(){
        return $this->hasOne(Supplier::class, 'id', 'supplier_id');
    }
}
