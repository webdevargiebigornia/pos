<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ingredient;
use App\Item;
use App\Product;

class StoreController extends Controller
{
    //
    public function search(Request $request)
    {
        $data = [];
        $products = Product::where("name","LIKE","%{$request->input('query')}%")->get();

        foreach($products as $product){

            if($product->available($request->quantity)){
                unset($product['ingredients']);
                $data[]= $product;
            }
        }
        
        return $data; 
        
        // if(isset($request->selected_items)){
        //     $item->whereNotIn('item_no', $request->selected_items);
        // }
        
        // if(is_null($item->first())){
        //     return response()->json('Item already in list or does not exists!', 404);
        // }
        
        // return $item->first();
        
    }

    public function autocomplete(Request $request)
    {
        $data = [];
        $products = Product::where("name","LIKE","%{$request->input('query')}%")->get();

        foreach($products as $product){

            if($product->available($request->quantity)){
                unset($product['ingredients']);
                $data[]= $product['name'];
            }
        }
        
        return $data; 
        
        // if(isset($request->selected_items)){
        //     $item->whereNotIn('item_no', $request->selected_items);
        // }
        
        // if(is_null($item->first())){
        //     return response()->json('Item already in list or does not exists!', 404);
        // }
        
        // return $item->first();
        
    }


}
