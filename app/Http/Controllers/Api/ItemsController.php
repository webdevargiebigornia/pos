<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Item;

class ItemsController extends Controller
{
    public function search(Request $request)
    {
        $item = Item::query()->with(['supplier', 'category'])->where('item_no', $request->item_no);
        
        if(isset($request->selected_items)){
            $item->whereNotIn('item_no', $request->selected_items);
        }
        
        if(is_null($item->first())){
            return response()->json('Item already in list or does not exists!', 404);
        }
        
        return $item->first();
        
    }

}
