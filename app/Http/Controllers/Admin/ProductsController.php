<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ingredient;
use App\Product;
use App\ProductCategory;
use App\Traits\Controllers\ResourceController;

class ProductsController extends Controller
{
    // 
    use ResourceController;

    /**
     * @var string
     */
    protected $resourceAlias = 'admin.products';

    /**
     * @var string
     */
    protected $resourceRoutesAlias = 'admin::products';

    /**
     * Fully qualified class name
     *
     * @var string
     */
    protected $resourceModel = Product::class;

    /**
     * @var string
     */
    protected $resourceTitle = 'Products';

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', $this->getResourceModel());
        $product_categories = ProductCategory::all();
        $class = $this->getResourceModel();
        return view($this->filterCreateView('_resources.create'), $this->filterCreateViewData([
            'record' => new $class(),
            'product_categories' => $product_categories,
            'resourceAlias' => $this->getResourceAlias(),
            'resourceRoutesAlias' => $this->getResourceRoutesAlias(),
            'resourceTitle' => $this->getResourceTitle(),
        ]));
    }
    /**
     * Used to validate store.
     *
     * @return array
     */
    private function resourceStoreValidationData()
    {
        return [
            'rules' => [
                'name' => 'required|min:3|max:255',
                'product_category_id' => 'required|in:'.ProductCategory::pluck('id')->implode(','),
                'price' => 'required',
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit($id)
    {
        $product_categories = ProductCategory::all();
        $record = $this->getResourceModel()::findOrFail($id);

        $this->authorize('update', $record);

        return view($this->filterEditView('_resources.edit'), $this->filterEditViewData($record, [
            'record' => $record,
            'product_categories' => $product_categories,
            'resourceAlias' => $this->getResourceAlias(),
            'resourceRoutesAlias' => $this->getResourceRoutesAlias(),
            'resourceTitle' => $this->getResourceTitle(),
        ]));
    }
    /**
     * Used to validate update.
     *
     * @param $record
     * @return array
     */
    private function resourceUpdateValidationData($record)
    {
        return [
            'rules' => [
                'name' => 'required|min:3|max:255',
                'product_category_id' => 'required|in:'.ProductCategory::pluck('id')->implode(','),
                'price' => 'required',
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param null $record
     * @return array
     */
    private function getValuesToSave(Request $request, $record = null)
    {
        $creating = is_null($record);
        $values = [];
        $values['name'] = $request->input('name', '');
        $values['product_category_id'] = $request->input('product_category_id', '');
        $values['price'] = $request->input('price', '');
        return $values;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', $this->getResourceModel());

        $valuesToSave = $this->getValuesToSave($request);
        $request->merge($valuesToSave);
        $this->resourceValidate($request, 'store');

        if ($record = $this->getResourceModel()::create($this->alterValuesToSave($request, $valuesToSave))) {
            
            if($request->item_id){
                foreach($request->item_id as $key => $item_id){
                    $ingredient = new Ingredient;
                    $ingredient->product_id = $record->id;
                    $ingredient->item_id = $item_id;
                    $ingredient->quantity = $request->quantity[$key];
                    $ingredient->save();
                }
            }
            

            flash()->success('Element successfully inserted.');
            return $this->getRedirectAfterSave($record);
        } else {
            flash()->info('Element was not inserted.');
        }

        return $this->redirectBackTo(route($this->getResourceRoutesAlias().'.index'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $record = $this->getResourceModel()::findOrFail($id);

        $this->authorize('update', $record);

        $valuesToSave = $this->getValuesToSave($request, $record);
        $request->merge($valuesToSave);
        $this->resourceValidate($request, 'update', $record);

        if ($record->update($this->alterValuesToSave($request, $valuesToSave))) {
            $record->ingredients()->delete();
            if($request->item_id){
                foreach($request->item_id as $key => $item_id){
                    $ingredient = new Ingredient;
                    $ingredient->product_id = $record->id;
                    $ingredient->item_id = $item_id;
                    $ingredient->quantity = $request->quantity[$key];
                    $ingredient->save();
                }
            }
            
            flash()->success('Element successfully updated.');

            return $this->getRedirectAfterSave($record);
        } else {
            flash()->info('Element was not updated.');
        }

        return $this->redirectBackTo(route($this->getResourceRoutesAlias().'.index'));
    }


    private function alterValuesToSave(Request $request, $values)
    {

        return $values;
    }

    /**
     * @param $record
     * @return bool
     */
    private function checkDestroy($record)
    {
        if (Auth::user()->id == $record->id) {
            flash()->error('You can not delete your own user.');

            return false;
        }

        return true;
    }

    /**
     * Retrieve the list of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $perPage
     * @param string|null $search
     * @return \Illuminate\Support\Collection
     */
    private function getSearchRecords(Request $request, $perPage = 15, $search = null)
    {
        return $this->getResourceModel()::when(! empty($search), function ($query) use ($search) {
            $query->where(function ($query) use ($search) {
                $query->where('name', 'like', "%$search%");
            });
        })->paginate($perPage);
    }

}
