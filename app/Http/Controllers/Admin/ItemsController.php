<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Item;
use App\Supplier;
use App\Traits\Controllers\ResourceController;
use App\Unit;

class ItemsController extends Controller
{
    // 
    use ResourceController;

    /**
     * @var string
     */
    protected $resourceAlias = 'admin.items';

    /**
     * @var string
     */
    protected $resourceRoutesAlias = 'admin::items';

    /**
     * Fully qualified class name
     *
     * @var string
     */
    protected $resourceModel = Item::class;

    /**
     * @var string
     */
    protected $resourceTitle = 'items';

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $this->authorize('viewList', $this->getResourceModel());

        $paginatorData = [];
        $perPage = (int) $request->input('per_page', '');
        $perPage = (is_numeric($perPage) && $perPage > 0 && $perPage <= 100) ? $perPage : 15;
        if ($perPage != 15) {
            $paginatorData['per_page'] = $perPage;
        }
        $search = trim($request->input('search', ''));
        if (! empty($search)) {
            $paginatorData['search'] = $search;
        }
        $records = $this->getSearchRecords($request, $perPage, $search);
        $records->appends($paginatorData);

        return view($this->filterIndexView('_resources.index'), $this->filterSearchViewData($request, [
            'records' => $records,
            'search' => $search,
            'resourceAlias' => $this->getResourceAlias(),
            'resourceRoutesAlias' => $this->getResourceRoutesAlias(),
            'resourceTitle' => $this->getResourceTitle(),
            'perPage' => $perPage,
        ]));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $suppliers = Supplier::all();
        $units = Unit::all();
        $categories = Category::all();

        $this->authorize('create', $this->getResourceModel());

        $class = $this->getResourceModel();
        return view($this->filterCreateView('_resources.create'), $this->filterCreateViewData([
            'suppliers' => $suppliers,
            'units' => $units,
            'categories' => $categories,
            'record' => new $class(),
            'resourceAlias' => $this->getResourceAlias(),
            'resourceRoutesAlias' => $this->getResourceRoutesAlias(),
            'resourceTitle' => $this->getResourceTitle(),
        ]));
    }
    /**
     * Used to validate store.
     *
     * @return array
     */
    private function resourceStoreValidationData()
    {
        return [
            'rules' => [
                'item_no' => 'required|unique:items',
                'name' => 'required|min:3|max:255',
                'supplier_id' => 'required|in:'.Supplier::pluck('id')->implode(','),
                'unit_id' => 'required|in:'.Unit::pluck('id')->implode(','),
                'category_id' => 'required|in:'.Category::pluck('id')->implode(','),
                'expiry_date' => 'nullable|date',
                'date_received' => 'required|date',
                'cost' => 'required',
                'cost_per_quantity' => 'required',
                'quantity' => 'required',
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit($id)
    {
        $record = $this->getResourceModel()::findOrFail($id);
        $suppliers = Supplier::all();
        $units = Unit::all();
        $categories = Category::all();
        $this->authorize('update', $record);

        return view($this->filterEditView('_resources.edit'), $this->filterEditViewData($record, [
            'record' => $record,
            'suppliers' => $suppliers,
            'units' => $units,
            'categories' => $categories,
            'resourceAlias' => $this->getResourceAlias(),
            'resourceRoutesAlias' => $this->getResourceRoutesAlias(),
            'resourceTitle' => $this->getResourceTitle(),
        ]));
    }

    
    /**
     * Used to validate update.
     *
     * @param $record
     * @return array
     */
    private function resourceUpdateValidationData($record)
    {

        return [
            'rules' => [
                'item_no' =>'required|unique:items,item_no,'.$record->id,
                'name' => 'required|min:3|max:255',
                'supplier_id' => 'required|in:'.Supplier::pluck('id')->implode(','),
                'unit_id' => 'required|in:'.Unit::pluck('id')->implode(','),
                'category_id' => 'required|in:'.Category::pluck('id')->implode(','),
                'expiry_date' => 'nullable|date',
                'date_received' => 'required|date',
                'cost' => 'required',
                'cost_per_quantity' => 'required',
                'quantity' => 'required',
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param null $record
     * @return array
     */
    private function getValuesToSave(Request $request, $record = null)
    {
        $creating = is_null($record);
        $values = [];
        $values['item_no'] = $request->input('item_no');
        $values['name'] = $request->input('name');
        $values['supplier_id'] = $request->input('supplier_id');
        $values['unit_id'] = $request->input('unit_id');
        $values['category_id'] = $request->input('category_id');
        $values['expiry_date'] = $request->input('expiry_date');
        $values['date_received'] = $request->input('date_received');
        $values['cost'] = $request->input('cost');
        $values['cost_per_quantity'] = $request->input('cost_per_quantity');
        $values['quantity'] = $request->input('quantity');

        return $values;
    }

    private function alterValuesToSave(Request $request, $values)
    {

        return $values;
    }

    /**
     * @param $record
     * @return bool
     */
    private function checkDestroy($record)
    {
        if (Auth::user()->id == $record->id) {
            flash()->error('You can not delete your own user.');

            return false;
        }

        return true;
    }

    /**
     * Retrieve the list of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $perPage
     * @param string|null $search
     * @return \Illuminate\Support\Collection
     */
    private function getSearchRecords(Request $request, $perPage = 15, $search = null)
    {
        return $this->getResourceModel()::when(! empty($search), function ($query) use ($search) {
            $query->where(function ($query) use ($search) {
                $query->where('name', 'like', "%$search%");
            });
        })->paginate($perPage);
    }

}
