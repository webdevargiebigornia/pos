<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Models\Impersonator;
use App\Traits\Eloquent\OrderableTrait;
use App\Traits\Eloquent\SearchLikeTrait;
use App\Traits\Models\FillableFields;
use Illuminate\Notifications\Notifiable;

class Product extends Model
{
    //
    use Notifiable, FillableFields, OrderableTrait, SearchLikeTrait, Impersonator;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'product_category_id',
        'price',
        'description',
    ];

    /**
     * @return mixed
     */
    public function getRecordTitle()
    {
        return $this->name;
    }

    public function ingredients(){
        return $this->hasMany(Ingredient::class, 'product_id' ,'id');
    }
    
    public function available($quantity){
        foreach($this->ingredients as $ingredient){
            
            if($ingredient->item->quantity < ($ingredient->quantity * $quantity)){
                return false;
            }
        }
        return true;
    }
}
