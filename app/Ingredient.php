<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Models\Impersonator;
use App\Traits\Eloquent\OrderableTrait;
use App\Traits\Eloquent\SearchLikeTrait;
use App\Traits\Models\FillableFields;
use Illuminate\Notifications\Notifiable;

class Ingredient extends Model
{
    //
    use Notifiable, FillableFields, OrderableTrait, SearchLikeTrait, Impersonator;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'item_id',
        'quantity',
    ];

    /**
     * @return mixed
     */
    public function getRecordTitle()
    {
        return $this->name;
    }

    public function item(){
        return $this->hasOne(Item::class,'id', 'item_id');
    }

}
